import { IResult, IStory } from "../types/types";

export const shuffleResults = (results: IResult[]) => {
  return results.sort((a: IResult, b: IResult) => 0.5 - Math.random());
};

export const shuffleStories = (stories: IStory[]) => {
  return stories.sort((a: IStory, b: IStory) => 0.5 - Math.random());
};
