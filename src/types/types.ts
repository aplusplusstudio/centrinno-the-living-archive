import type { TypedObject } from "@portabletext/types"

export interface IStory {
  [x: string]: any
  author: string
  mainText: string
  pilot: string
  publicationDate: string
  storyRelevance: string
  summaryText: string
  title: string
  _id: string
  heroImage: IImage
  locations: string[]
  tags: IPreference[]
  peopleInvolved: string[]
  relatedStories: IStory[]
}

export interface IImage {
  title: string
  asset: {
    _ref: string
  }
}

export interface IPreference {
  title: string
  definition: string
  category?: {
    title: string
    definition: string
  }
  relatedTags?: IPreference[]
}
export interface Itest {
  category: string
  definition: string
}

export interface IContext {
  stories: IStory[]
  about?: IAbout
  tags: IPreference[]
  categories: IPreference[]
  loader: boolean
}

export interface IResult {
  result: string
  isTag: boolean
  isSelected?: boolean
}

export interface DNode {
  children?: DNode[]
  data?: DData
  x?: number
  y?: number
  fx?: number
  fy?: number
  height?: number
  index?: number
  vx?: number
  vy?: number
  source?: { x?: number; y?: number }
  distance?: number
  class?: string
}

export interface DLink {
  source?: { x?: number; y?: number }
  target?: { x?: number; y?: number }
  distance?: number
}

export interface DData {
  name?: string
  id?: number
  children?: DData[]
  x?: number
  y?: number
  class?: string
  heroImage?: string
  definition?: string
  summary?: string
}

export interface IBlock {
  _key: string
  _type: string
  header: string
  textContent: TypedObject
  imageOne: IImage
  imageTwo: IImage
  gallery: IImage[]
  audio: {
    asset: {
      _ref: string
    }
  }
  uploadedFile: {
    asset: {
      _ref: string
    }
  }
  videoFile: {
    asset: {
      _ref: string
    }
  }
  videoUrl: string
}

export interface IText {
  children: string[]
}

export interface IAbout {
  firstBlock: IBlock
  secondBlock: IBlock
  thirdBlock: IBlock
  fourthBlock: IBlock
  fifthBlock: IBlock
  aboutGallery: IBlock
  title: string
}

export interface INode {
  id: string | undefined
  class: string
  definition?: string
  image?: string
  summary?: string
  name?: string
}

export interface ILink {
  source: string | undefined
  target: string | undefined
}
