import { Link } from 'react-router-dom'
import { IImage } from '../../../types/types'
import ImgHandler from '../../ImgHandler'
import { HeroGroup } from './styles'
import { useRecoilState } from 'recoil'
import { isAnimationOnState } from '../../../helpers/atoms'
import React from 'react'

interface HeroProps {
  title?: string
  image?: IImage
  link?: string
  ref?: React.Ref<HTMLDivElement>
}

const Hero = React.forwardRef<HTMLDivElement, HeroProps>(
  ({ image, title, link }, ref) => {
    const [, setIsAnimationOn] = useRecoilState(isAnimationOnState)

    const clickHandler = () => {
      setIsAnimationOn(false)
    }

    return (
      <HeroGroup>
        <div className="half">
          <h1>{title && title}</h1>
        </div>
        <div className="half">
          {image && title && (
            <ImgHandler
              imgSrc={image}
              width={2000}
              altText={`${title.toLowerCase()}-hero-image`}
              noPlaceholder={true}
            />
          )}
        </div>
        {link && <Link to={link} onClick={clickHandler}></Link>}
      </HeroGroup>
    )
  }
)

export default Hero
