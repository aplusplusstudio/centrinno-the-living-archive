import styled from "styled-components";

export const TwoImagesSection = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
    padding: 32px;
`