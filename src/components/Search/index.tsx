import { ChangeEvent, useState } from 'react'
import { InputBox, SearchBox } from './styles'
import { AnimatePresence, motion } from 'framer-motion'
import MagnifierIcon from './MagnifierIcon'
import CloseIcon from './CloseIcon'
import { hidden, hide, show } from '../../helpers/animationVariants'
import { Button, backgroundColor, textColor, thirdColor } from '../../styles'
import { IContext, IResult } from '../../types/types'
import { shuffleResults } from '../../helpers/shuffle'
import { setTagButtons } from '../../helpers/tagButtons'
import { useCentrinnoContext } from '../../context/storyContext'

interface SearchProps {
  inputValue: string
  setInputValue: React.Dispatch<React.SetStateAction<string>>
  buttons: IResult[]
  setButtons: React.Dispatch<React.SetStateAction<IResult[]>>
  setRenderFlag: React.Dispatch<React.SetStateAction<boolean>>
  renderFlag: boolean
}

export const Search = ({
  inputValue,
  setInputValue,
  buttons,
  setButtons,
  setRenderFlag,
  renderFlag,
}: SearchProps) => {
  const [showClear, setShowClear] = useState(false)
  const { tags } = useCentrinnoContext() as IContext
  const searchHandler = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value)
    if (event.target.value.length > 0) {
      setShowClear(true)
    } else {
      setShowClear(false)
    }
  }

  const handleClear = () => {
    setInputValue('')
    setShowClear(false)
  }

  const clickHandler = () => {
    setRenderFlag(!renderFlag)
    setButtons(shuffleResults(buttons))
  }

  return (
    <>
      {buttons.length > 0 && (
        <SearchBox>
          <div className="line"></div>
          <div className="text">
            <h3>The Living Archive Network</h3>
            <p>
              The Living Archive explores the 9 pilot cities across Europe in
              their collection of stories with participatory heritage methods.
              This webspace brings together many stories collected by nine{' '}
              <a
                href="https://centrinno.eu/cities/"
                target="_blank"
                rel="noreferrer"
                style={{ textTransform: 'none', textDecoration: 'underline' }}
              >
                Fab City Hub
              </a>{' '}
              teams from all over Europe. The collection has been carefully
              assembled by these teams who have been learning about, co-creating
              and applying participatory heritage-making approaches, emotion
              networking methodology, oral history principles and creative
              perspective-taking.
              <br />
              <br />
              Select tags and categories to filter stories in the archive below.
              Explore their connections in the graph. Drag the nodes of the
              graph to display information about them, and use the buttons to
              adjust the zoom.
            </p>
          </div>
          <div className="wrapper">
            <div className="inner">
              <InputBox
                type="search"
                id="site-search"
                name="q"
                placeholder="search categories/tags"
                value={inputValue}
                onChange={searchHandler}
                autoComplete="one-time-code"
              />
              <AnimatePresence mode="wait">
                {showClear ? (
                  <motion.div
                    initial={hidden}
                    animate={show}
                    exit={hide}
                    key="icon"
                    className="icon close-icon"
                  >
                    <CloseIcon handleClear={handleClear} />
                  </motion.div>
                ) : (
                  <motion.div
                    initial={hidden}
                    animate={show}
                    exit={hide}
                    key="icon-square"
                    className="icon"
                  >
                    <MagnifierIcon />
                  </motion.div>
                )}
              </AnimatePresence>
            </div>
            <Button
              onClick={clickHandler}
              className="small no-hover"
              buttonColor="white"
              backgroundColor="white"
              buttonTextColor={textColor}
              border={0}
            >
              <p>shuffle</p>
            </Button>
          </div>
        </SearchBox>
      )}
    </>
  )
}
