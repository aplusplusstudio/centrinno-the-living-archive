#The Living Archive:

Connecting local stories from (post)-industrial sites to inspire a circular and inclusive future in Europe

Why
How to make our way to fit within planetary boundaries? This might be the greatest and most urgent challenge ever faced by humanity. This challenge is addressed by seeking to make the economy more circular and the society more inclusive. Underpinning all efforts is a quest to make the way we live together, with people and planet, regenerative instead of exhaustive. This quest touches all disciplines and approaches for change, and - precisely because change happens over time - the element of how we perceive and act in the now relates to how we imagine the future and how we are contextualized by the past, by stories.

Which stories we listen to, which we tell and retell, how and to who, plays a key role in the transition. So which stories matter? How do they relate to one another? What do these relations indicate?

In the CENTRINNO project, nine cities have worked on and around (post-)industrial sites to establish and foster new ways of making, creating in the process so-called Fab City Hubs. How do the people who create these hubs explore, find, utilise and harness traces of the past, in the present, toward the future? What stories do they give space? Which layers and meanings do they attribute?

What
The Living Archive is an open access platform containing content (stories) stemming from (post-) industrial sites, collected locally with participatory heritage methods. Its purpose is to help communities imagine what can be broadly described as a new ‘critical heritage of making’, and enable the creation of inclusive and circular hubs (Fab City Hubs).

Such a critical heritage approach aims to indicate the multi-layered, complex and sometimes conflicting nature of heritage items. These, in turn, aim to stimulate discussions on the role of the past in the present, and how to use these as inspirations for a more sustainable, socially inclusive, and circular future. The intentions of the Living Archive thus align with the principles of the Fab City Global Initiative. Nonetheless, the methodology and ethos behind the Living Archive should be replicable and disseminable to other projects wishing to implement participatory heritage methods into their research and innovation practice.

How
The content found within the Living Archive consists of many different types of data, which has been co-collected in a participatory manner. Stories include text, visual imagery, artistic work, audio recordings, oral histories, and beyond. These stories have been collected via the participatory heritage methodologies utilised and disseminated by the AHK and another partner project, Imagine IC, also in Amsterdam. Three (sometimes intersecting) main methods for collecting this content are used: Emotion Networking, ethnographic fieldwork, and interviews, each of which have ‘how-to’ guides embedded within the general CENTRINNO Living Archive gitbook.

Emotion Networking, a methodology developed by the Reinwardt Academy, part of the AHK, and Imagine IC, brings together different stakeholders to collectively discuss and map their emotional responses to an item of discussion which could be understood as heritage - for example a bridge, a knitting practice, a polluted lake, a flag, a flower, etc - before adjusting their position based on the testimony of others. Several rounds take place, including the imagined perspective of absent stakeholders and the introduction of additional information, so as to give as wide a perspective on the (prospective) heritage item as possible.

Ethnographic fieldwork consists of exploring the site via communicating with people and recording sights, sounds, smells, and so on, and then documenting what happens through these ethnographic excursions.

Oral Histories and/or Interviews - undertaken with makers, local inhabitants, policy makers, creatives, business owners, former industrial workers, and so on - allow people with particular stories about the area the space to explain their understanding of the site, its history, the interrelation between past, present and future, or their personal relationship to it.

Once it has been collected - which often first takes place on the local level in the form of an exhibition - it is assigned tags according to notable characteristics, formulated into a narrative, and uploaded to the digital infrastructure of the Living Archive. This then shows both the geographical placement of the story and also how it connects to other collected stories which can be associated via one of these assigned tags. We hope you enjoy exploring these heritage items and their network of relations!

Disclaimer: The Living Archive is made up of content collected by the local pilot teams from within the CENTRINNO project’s 9 locations. Every piece of content has been collected with the due consent from the people involved in the stories. Image credit is given and, where relevant, copyright is issued under CC0 licence. If you are the owner of or dispute the usage of the any of the content found on the CENTRINNO Living Archive, please contact jonathan.even-zohar@ahk.nl

Who
The Living Archive is the result of work within the Horizon-funded CENTRINNO project, and has been conceptualised and developed by the Reinwardt Academy, part of the Amsterdam University of the Arts (AHK) and other partners within the CENTRINNO project, in conjunction with designer and developer Alessandro Amato at A++ Studio, using the sanity.io web database and content management system. Other relevant places within the CENTRINNO project are the CENTRINNO Framework, the CENTRINNO Fab City Hub Toolkit, the CENTRINNO Cartography, CENTRINNO Blueprints, and the CENTRINNO Schools initiative.

When
The Living Archive was developed between 2020-2022, and is launched in 2023.
